package ui.pages;

import com.codeborne.selenide.HoverOptions;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.time.Duration;
import java.util.NoSuchElementException;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class DashboardPage {

    SelenideElement userTopBar = $x("//li[@class='oxd-userdropdown']");
    SelenideElement helpButton = $x("//button[@class='oxd-icon-button']");
    SelenideElement sidebarSearchString = $x("//input[contains(@class, 'oxd-input')]");
    SelenideElement sidebarContainer = $x("//ul[@class='oxd-main-menu']");
    SelenideElement sidebarMenuItemText = $x("//span[contains(@class, 'oxd-main')]");
    SelenideElement employeesConfigurationGear = $x("//i[contains(@class, 'bi-gear-fill')]");
    SelenideElement successUpdatedContainer = $x("//div[contains(@class, 'oxd-toast--success')]");
    SelenideElement pieTooltip = $x("//span[@id='oxd-pie-chart-tooltip']");
    SelenideElement subUnitPie = $x("//div[6]/*/div[2]//div[@class='oxd-pie-chart']");
    SelenideElement timeButton = $x("//button/i[contains(@class, 'bi-stop')]");
    SelenideElement sidebarMenuItem = $x("//ul[@class='oxd-main-menu']");


    @Step("Проверить отображение залогиненного пользователя")
    public void checkUserTopBar(boolean displayed) {
        if (displayed) {
            userTopBar.shouldBe(visible);
        } else {
            userTopBar.shouldBe(hidden);
        }
    }

    @Step("Нажать на кнопку 'Help'")
    public void clickHelpButton() {
        helpButton.click();
    }

    @Step("Проверить отображение кнопки 'Help' и её подсказки")
    public void checkHelpButton() {
        helpButton.shouldBe(visible).shouldHave(attribute("title", "Help"));
    }

    @Step("Выполнить поиск в боковом меню")
    public void searchBySidebarSearchString(String menuItem) {
        sidebarSearchString.sendKeys(menuItem);
    }

    @Step("Проверить отображение пункта в боковом меню")
    public void checkSidebarMenuItem(String item) {
        sidebarContainer.shouldHave(text(item));
        sidebarMenuItemText.shouldBe(visible);
    }

    @Step("Кликнуть на шестерёнку настроек 'Employees on Leave Today'")
    public void clickEmpoyeesGear() {
        employeesConfigurationGear.click();
    }

    @Step("Проверить сообщение об успешном сохранении изменений")
    public void checkEmployeesOptionsSuccess() {
        successUpdatedContainer.shouldBe(visible, Duration.ofSeconds(2));
    }

    @Step("Нажать на текст легенды графика")
    public void clickLegendText(String legend) {
        SelenideElement legendText = $x("//span[contains(@title, '" + legend + "')]");
        legendText.click();
    }

    @Step("Проверить перечёркивание легенды по нажатию")
    public void checkLegendTooltipLine(String legend) {
        SelenideElement legendText = $x("//span[contains(@title, '" + legend + "')]");
        legendText.hover().click();
        legendText.shouldHave(attribute("style","text-decoration: line-through;"));
    }

    @Step("Проверить отображение тултипа при наведении на график")
    public void checkPieTooltip() {
        try {
            subUnitPie.hover(HoverOptions.withOffset(10,0));
            pieTooltip.shouldBe(appear, Duration.ofSeconds(5));
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    @Step("Нажать кнопку 'Time'")
    public void clickTimeButton() {
        timeButton.click();
    }

    @Step("Перейти в раздел через боковое меню")
    public void followSidebarMenuItem(String menuItem) {
        searchBySidebarSearchString(menuItem);
        sidebarMenuItemText.click();
    }
}
