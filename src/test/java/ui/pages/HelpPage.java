package ui.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class HelpPage {

    SelenideElement searchString = $x("//input[@name='query']");
    SelenideElement resultSubheader = $x("//h1[@class='search-results-subheading']");
    SelenideElement categoriesContainer = $x("//section[@class='categories blocks']");

    @Step("Открыть страницу 'Help'")
    public void openHelpPage(){
        open("https://starterhelp.orangehrm.com/hc/en-us");
    }

    @Step("Выполнить запрос с помощью строки поиска")
    public void searchBySearchString(String input) {
        searchString.sendKeys(input);
        searchString.pressEnter();
    }

    @Step("Проверить текст заголовка с результатами поиска")
    public void checkResultSubheaderText(String subheaderText) {
        resultSubheader.shouldHave(text(subheaderText));
    }

    @Step("Проверить наличие категории в списке")
    public void checkHelpCategory(String category) {
        categoriesContainer.should(exist);
        categoriesContainer.shouldHave(text(category));
    }
}
