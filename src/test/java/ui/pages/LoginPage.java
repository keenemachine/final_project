package ui.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {

    SelenideElement usernameInput = $x("//input[@name='username']");
    SelenideElement passwordInput = $x("//input[@name='password']");
    SelenideElement submitButton = $x("//button[@type='submit']");
    SelenideElement forgotPasswordLink = $x("//div[@class='orangehrm-login-forgot']");
    SelenideElement invalidCredentialsAlert = $x("//div[@role='alert']");
    SelenideElement successResetMessageContainer = $x("//div[@class='orangehrm-card-container']");

    @Step("Открыть страницу логина")
    public void openLoginPage() {
        open("https://opensource-demo.orangehrmlive.com/");
    }

    @Step("Ввести логин")
    public void setUsername(String username) {
        usernameInput.sendKeys(username);
    }

    @Step("Ввести логин и пароль")
    public void setCredentials(String username, String password) {
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
    }

    @Step("Нажать кнопку 'Login/Submit/Reset'")
    public void clickSubmitButton() {
        submitButton.click();
    }

    @Step("Проверить отображение ошибки логина")
    public void checkLoginAlert(boolean displayed) {
        if (displayed) {
            invalidCredentialsAlert.shouldBe(visible);
            invalidCredentialsAlert.shouldHave(text("Invalid credentials"));
        } else {
            invalidCredentialsAlert.shouldBe(hidden);
        }
    }

    @Step("Нажать на ссылку перехода к восстановлению пароля")
    public void clickForgotPasswordLink() {
        forgotPasswordLink.click();
    }

    @Step("Проверить отображение сообщения 'Reset Password link sent successfully'")
    public void checkSuccessResetMessage(boolean displayed) {
        if (displayed) {
            successResetMessageContainer.shouldBe(visible);
            successResetMessageContainer.shouldHave(text("Reset Password link sent successfully"));
        } else {
            successResetMessageContainer.shouldBe(hidden);
        }
    }

    @Step("Зайти в систему как Admin")
    public void loginAsAdmin() {
        openLoginPage();
        usernameInput.sendKeys("Admin");
        passwordInput.sendKeys("admin123");
        clickSubmitButton();
    }
}
