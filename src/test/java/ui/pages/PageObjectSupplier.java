package ui.pages;

public interface PageObjectSupplier {

    default LoginPage loginPage() {
        return new LoginPage();
    }

    default DashboardPage dashboardPage() {
        return new DashboardPage();
    }

    default HelpPage helpPage() {
        return new HelpPage();
    }

    default TimePage timePage() {
        return new TimePage();
    }
}
