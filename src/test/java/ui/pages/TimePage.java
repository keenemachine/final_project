package ui.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.time.Duration;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class TimePage {

    SelenideElement topBarHeader = $x("//span[@class='oxd-topbar-header-breadcrumb']");
    SelenideElement sectionContainer = $x("//div[@class='orangehrm-card-container']");
    SelenideElement dateInput = $x("//input[@placeholder='yyyy-mm-dd']");
    SelenideElement timeInput = $x("//input[@placeholder='hh:mm']");
    SelenideElement noteInput = $x("//textarea[@placeholder='Type here']");
    SelenideElement attendanceActionContainer = $x("//div[@class='oxd-form-actions']");
    SelenideElement dateContainer = $x("//div[1][contains(@class, 'oxd-grid-item--gutters --offset-row-2')]");
    SelenideElement timeContainer = $x("//div[2][contains(@class, 'oxd-grid-item--gutters --offset-row-2')]");
    SelenideElement addCustomerButton = $x("//i[contains(@class, 'bi-plus')]");
    SelenideElement customerDescriptionInput = $x("//textarea[@placeholder='Type description here']");
    SelenideElement customerNameInput = $x("//div[2]/input[contains(@class, 'oxd-input--active')]");
    SelenideElement customerRowGroup = $x("//div[@role='table']");
    SelenideElement customerCancelButton = $x("//button[contains(@class, 'oxd-button--ghost')]");


    @Step("Проверить отображение заголовка раздела")
    public void checkTopBarHeader(String header) {
        topBarHeader.shouldHave(text(header));
    }

    @Step("Проверить заголовок в теле раздела")
    public void checkSectionTitle(String title) {
        sectionContainer.shouldHave(text(title));
    }

    @Step("Заполнить поле 'Date'")
    public void editDate(String date) {
        dateInput.sendKeys(date);
    }

    @Step("Заполнить поле 'Time'")
    public void editTime(String time) {
        timeInput.sendKeys(time);
    }

    @Step("Заполнить поле 'Note'")
    public void editNote(String note) {
        noteInput.sendKeys(note);
    }

    @Step("Перейти в раздел из пункта меню топ-бара")
    public void followTopbarMenuItem(String topbarItemName, String menuItemName) {
        SelenideElement topbarItem = $x("//span[contains(text(),'" + topbarItemName + "')]");
        SelenideElement menuItem = $x("//a[contains(text(),'" + menuItemName + "')]");
        topbarItem.click();
        menuItem.click();
    }

    @Step("Проверить текст кнопки 'Submit'")
    public void checkSubmitButtonText(String submitButtonText) {
        attendanceActionContainer.shouldHave(text(submitButtonText));
    }

    @Step("Проверить отображение предупреждение для поля 'Date'")
    public void checkDateAlert(String alertText) {
        dateContainer.shouldHave(text(alertText));
    }

    @Step("Проверить отображение предупреждения для поля 'Time'")
    public void checkTimeAlert(String alertText) {
        timeContainer.shouldHave(text(alertText));
    }

    @Step("Очистить поля 'Date' и 'Time'")
    public void clearDateAndTime() {
        dateInput.clear();
        timeInput.clear();
    }

    @Step("Нажать 'Добавить клиента'")
    public void clickAddCustomer() {
        addCustomerButton.click();
    }

    @Step("Заполнить описание клиента")
    public void editCustomerDescription(String customerDescription) {
        customerDescriptionInput.sendKeys(customerDescription);
    }

    @Step("Заполнить имя клиента")
    public void editCustomerName(String customerName) {
        customerNameInput.sendKeys(customerName);
    }

    @Step("Проверить отображение добавленного клиента")
    public void checkCustomerInList(String customerName, String customerDescription) {
        customerRowGroup.shouldHave(text(customerName), Duration.ofSeconds(10));
        customerRowGroup.shouldHave(text(customerDescription), Duration.ofSeconds(10));
    }

    @Step("Проверить отсутствие добавленного клиента")
    public void checkAbsenceCustomerInList(String customerName, String customerDescription) {
        customerRowGroup.shouldNotHave(text(customerName), Duration.ofSeconds(7));
        customerRowGroup.shouldNotHave(text(customerDescription), Duration.ofSeconds(7));
    }

    @Step("Нажать 'Cancel' в форме добавления клиента")
    public void clickCancelCustomerButton() {
        customerCancelButton.click();
    }
}
