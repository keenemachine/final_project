package ui.tests;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

import java.util.Map;

public class DashboardTests implements PageObjectSupplier {

    @BeforeSuite
    public void setRemote() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        Configuration.remote = "http://51.250.123.179:4444/wd/hub";
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
        Configuration.browserVersion = "112.0";
        Configuration.browserCapabilities = capabilities;
    }

    @BeforeClass
    public void setUp() {
        loginPage().loginAsAdmin();
    }

    @Test(description = "Поиск пункта меню в сайдбаре")
    public void checkSidebarSearch() {
        String menuItem = "Claim";
        dashboardPage().searchBySidebarSearchString(menuItem);
        dashboardPage().checkSidebarMenuItem(menuItem);
    }

    @Test(description = "Отображение всплывающего окна успешного применения изменений")
    public void checkEmployeesOptionsSuccess() {
        dashboardPage().clickEmpoyeesGear();
        loginPage().clickSubmitButton();
        dashboardPage().checkEmployeesOptionsSuccess();
    }

    @Test(description = "Отображение перечёркивания тултипа по нажатию под графиком дашборда")
    public void checkLegendTooltipLine() {
        dashboardPage().checkLegendTooltipLine("Human Resources");
    }

    @Test(description = "Отображение тултипа при наведении на график")
    public void checkPieTooltip() {
        dashboardPage().clickLegendText("Unassigned");
        dashboardPage().checkPieTooltip();
    }

    @Test(description = "Переход в раздел 'Time' по кнопке на дашборде")
    public void checkTimeButtonFollowingLink() {
        dashboardPage().clickTimeButton();
        timePage().checkTopBarHeader("Attendance");
        timePage().checkSectionTitle("Punch Out");
    }
}
