package ui.tests;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

import java.util.Map;

public class LoginTests implements PageObjectSupplier {

    @BeforeSuite
    public void setRemote() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        Configuration.remote = "http://51.250.123.179:4444/wd/hub";
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
        Configuration.browserVersion = "112.0";
        Configuration.browserCapabilities = capabilities;
    }

    @BeforeClass
    public void setUp() {
        loginPage().openLoginPage();
    }

    @Test(description = "Вход в систему валидным пользователем")
    public void positiveLogin() {
        loginPage().setCredentials("Admin", "admin123");
        loginPage().clickSubmitButton();
        dashboardPage().checkUserTopBar(true);
    }

    @Test(description = "Вход в систему невалидным пользователем")
    public void negativeLogin() {
        loginPage().setCredentials("vasily", "1337");
        loginPage().clickSubmitButton();
        loginPage().checkLoginAlert(true);
    }

    @Test(description = "Восстановление пароля пользователя")
    public void resetPassword() {
        loginPage().clickForgotPasswordLink();
        loginPage().setUsername("igor");
        loginPage().clickSubmitButton();
        loginPage().checkSuccessResetMessage(true);
    }
}
