package ui.tests;

import com.codeborne.selenide.Configuration;
import com.github.javafaker.Faker;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

import java.util.Map;

public class TimeTests implements PageObjectSupplier {

    public static Faker faker = new Faker();

    @BeforeSuite
    public void setRemote() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        Configuration.remote = "http://51.250.123.179:4444/wd/hub";
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
        Configuration.browserVersion = "112.0";
        Configuration.browserCapabilities = capabilities;
    }

    @BeforeClass
    public void setUp() {

        loginPage().loginAsAdmin();
        dashboardPage().followSidebarMenuItem("Time");
    }

    @Test(description = "Валидация формы после заполнения полей 'Дата' и 'Время'")
    public void dateAndTimeEditing() {
        timePage().followTopbarMenuItem("Attendance", "Punch In/Out");
        timePage().editDate("2024-05-31");
        timePage().editTime("09:38 AM");
        timePage().checkSubmitButtonText("In");
    }

    @Test(description = "Валидация формы после заполнения поля 'Примечание'")
    public void noteEditing() {
        timePage().followTopbarMenuItem("Attendance", "Punch In/Out");
        timePage().editNote(faker.rickAndMorty().quote());
        timePage().checkSubmitButtonText("In");
    }

    @Test(description = "Предупреждения пи наличии незаполеннных полей 'Дата' и 'Время'")
    public void checkDateAndTimeAlerts() {
        timePage().followTopbarMenuItem("Attendance", "Punch In/Out");
        timePage().clearDateAndTime();
        loginPage().clickSubmitButton();
        timePage().checkDateAlert("Required");
        timePage().checkTimeAlert("Required");
    }

    @Test(description = "Добавление нового кандидата")
    public void addNewCustomer() {
        String customerName = faker.name().firstName();
        String customerDescription = "Страшный человек";
        timePage().followTopbarMenuItem("Project Info", "Customers");
        timePage().clickAddCustomer();
        timePage().editCustomerName(customerName);
        timePage().editCustomerDescription(customerDescription);
        loginPage().clickSubmitButton();
        timePage().checkCustomerInList(customerName, customerDescription);
    }

    @Test(description = "Отмена добавления нового кандидата")
    public void cancelAddingNewCustomer() {
        String customerName = faker.name().firstName();
        String customerDescription = "Звонарь!";
        timePage().followTopbarMenuItem("Project Info", "Customers");
        timePage().clickAddCustomer();
        timePage().editCustomerName(customerName);
        timePage().editCustomerDescription(customerDescription);
        timePage().clickCancelCustomerButton();
        timePage().checkAbsenceCustomerInList(customerName, customerDescription);
    }
}
