package api.tests;

import api.models.Pet;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static api.tests.RandomizeJson.random;
import static api.tests.RandomizeJson.statusQueryParams;
import static org.hamcrest.Matchers.equalTo;

public class PetTests extends BaseTest {

    @DataProvider(name = "petData")
    public Object[][] createdPetId() {
        Pet pet = RestAssured
                .given()
                .spec(baseGetNoLogSpecification())
                .body(RandomizeJson.randomizeJsonPet())
                .when()
                .post("/pet")
                .then()
                .spec(baseResponseNoLogSpecification())
                .extract()
                .body()
                .jsonPath()
                .getObject("$", Pet.class);

        return new Object[][]{
                {pet.id, pet.name},
        };
    }

    @DataProvider(name = "idData")
    public Object[][] randomPetId() {
        List<Long> petList = RestAssured
                .given()
                .spec(baseGetNoLogSpecification())
                .queryParams(statusQueryParams)
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(baseResponseNoLogSpecification())
                .extract()
                .body()
                .jsonPath()
                .getList("$", Pet.class)
                .stream()
                .map(pet -> pet.id)
                .toList();

        String petId = petList.get(random.nextInt(0, petList.size() - 1)).toString();

        return new Object[][]{
                {petId}
        };
    }

    @Test(description = "Создание питомца")
    public void createPet() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .body(RandomizeJson.randomizeJsonPet())
                .when()
                .post("/pet")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .response();
    }

    @Test(description = "Получение информации о добавленном питомце",
            dataProvider = "petData")
    public void getCreatedPet(Long petId, String name) {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .pathParam("petId", petId.toString())
                .when()
                .get("/pet/{petId}")
                .then()
                .spec(baseResponseLogSpecification())
                .body("id", equalTo(petId))
                .body("name", equalTo(name));
    }

    @Test(description = "Удаление питомца",
            dataProvider = "idData")
    public void deletePet(String petId) {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .pathParam("petId", petId)
                .when()
                .delete("/pet/{petId}")
                .then()
                .spec(baseResponseLogSpecification())
                .body("message", equalTo(petId));
    }

    @Test(description = "Получение информации о несуществующем питомце")
    public void getNotCreatedPet() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .when()
                .get("/pet/22222222")
                .then()
                .statusCode(404);
    }

    @Test(description = "Удаление несуществующего питомца")
    public void deleteNotCreatedPet() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .when()
                .delete("/pet/12412541241")
                .then()
                .statusCode(404);
    }

    @Test(description = "Некорректный запрос к случайному питомцу",
            dataProvider = "idData")
    public void incorrectRequestByPetId(String petId) {
        RestAssured
                .given()
                .spec(sendImageSpecification())
                .formParam("additionalMetadata","test")
                .pathParam("petId", petId)
                .multiPart("", "")
                .when()
                .put("/pet/{petId}/omagad")
                .then()
                .statusCode(404);
    }

    @Test(description = "Изменение имени питомца",
            dataProvider = "idData")
    public void changeCreatedPetNameById(String petId) {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .contentType(ContentType.URLENC)
                .formParam("name", faker.name().name())
                .pathParam("petId", petId)
                .when()
                .post("/pet/{petId}")
                .then()
                .spec(baseResponseLogSpecification())
                .body("message", equalTo(petId));
    }

    @Test(description = "Проверка отсутствия прав доступа к изменению питомца")
    public void checkForbiddenAccessToPet() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .body("")
                .when()
                .put("/pet")
                .then()
                .statusCode(405);
    }

    @Test(description = "Загрузка изображения",
            dataProvider = "idData")
    public void uploadImageByPetId(String petId) {
        RestAssured
                .given()
                .spec(sendImageSpecification())
                .pathParam("petId", petId)
                .formParam("additionalMetadata","test")
                .multiPart("file", img, "image/jpeg")
                .when()
                .post("/pet/{petId}/uploadImage")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .response();
    }

    @Test(description = "Проверка соответствия выборки по статусу 'Pending'")
    public void assertGetPendingPet() {
       boolean pendingPet = RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .queryParams("status", "pending")
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .body()
                .jsonPath()
                .getList("$", Pet.class)
                .stream()
                .allMatch(pet -> pet.status.equals("pending"));

        Assert.assertTrue(pendingPet);
    }

    @Test(description = "Проверка выборки по некорректному статусу")
    public void getIncorrectStatusPet() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .queryParams("status", "Critical")
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(baseResponseLogSpecification())
                .body("message", Matchers.empty());
    }
}
