package api.tests;

import api.models.Pet;
import api.models.Store;
import io.restassured.RestAssured;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static api.tests.RandomizeJson.*;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;

public class StoreTests extends BaseTest {

    @DataProvider(name = "orderPetData")
    public Object[][] createdPetIdForOrder() {
        Pet pet = RestAssured
                .given()
                .spec(baseGetNoLogSpecification())
                .body(randomizeJsonPet())
                .when()
                .post("/pet")
                .then()
                .spec(baseResponseNoLogSpecification())
                .extract()
                .body()
                .jsonPath()
                .getObject("$", Pet.class);

        return new Object[][]{
                {pet.id},
        };
    }

    @DataProvider(name = "orderData")
    public Object[][] dataProviderFor() {
        Store store = RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .body(randomizeJsonPetForStore(idRandomizer(18)))
                .when()
                .post("/store/order")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .jsonPath()
                .getObject("$", Store.class);

        return new Object[][]{
                {store.id},
        };
    }

    @Test(description = "Создание заказа",
            dataProvider = "orderPetData")
    public void addOrderByPetId(Long id) {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .body(randomizeJsonPetForStore(id))
                .when()
                .post("/store/order")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .response();
    }

    @Test(description = "Удаление заказа",
            dataProvider = "orderData")
    public void deleteOrder(int storeId) {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .pathParam("orderId", storeId)
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .response();
    }

    @Test(description = "Создание заказа с невалидными данными")
    public void addNotCreatableOrder() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .body("привет!")
                .when()
                .post("/store/order")
                .then()
                .statusCode(400);
    }

    @Test(description = "Получение информации о добавленном заказе",
            dataProvider = "orderData")
    public void getOrder(int storeId) {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .pathParam("orderId", storeId)
                .when()
                .get("/store/order/{orderId}")
                .then()
                .spec(baseResponseLogSpecification())
                .extract()
                .response();
    }

    @Test(description = "Получение количества недобрых животных")
    public void getInventoryAlwaysAngry() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .when()
                .get("/store/inventory")
                .then()
                .spec(baseResponseLogSpecification())
                .body("'Always angry'", comparesEqualTo(2));
    }

    @Test(description = "Получение информации о несуществующем заказе")
    public void getNotCreatedOrder() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .pathParam("orderId", random.nextInt(1000, 10000))
                .when()
                .get("/store/order/{orderId}")
                .then()
                .statusCode(404);
    }

    @Test(description = "Удаление несуществующего заказа")
    public void deleteNotCreatedOrder() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .pathParam("orderId", random.nextInt(1000, 2000))
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .statusCode(404)
                .body("code", equalTo(404));
    }

    @Test(description = "Проверка отсутствия прав доступа к изменению заказа")
    public void checkForbiddenAccessToOrder() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .body("")
                .when()
                .put("/store/order")
                .then()
                .statusCode(405);
    }

    @Test(description = "Получение доступных методов для '/store/inventory'")
    public void getInventoryAllowedMethods() {
        RestAssured
                .given()
                .spec(baseGetLogSpecification())
                .when()
                .get("/store/inventory")
                .then()
                .spec(baseResponseLogSpecification())
                .header("Access-Control-Allow-Methods", equalTo("GET, POST, DELETE, PUT"));
    }
}
