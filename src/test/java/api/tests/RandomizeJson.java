package api.tests;

import api.models.Category;
import api.models.Pet;
import api.models.Store;
import api.models.Tag;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RandomizeJson {

    static ObjectMapper objectMapper = new ObjectMapper();
    static Random random = new Random();
    static Faker faker = new Faker();
    public static Map<String, List<String>> statusQueryParams = Map.of("status", List.of("available", "pending", "sold"));

    public static Long idRandomizer(int length) {
        StringBuilder id = new StringBuilder();
        for (int i = 0; i < length; i++) {
            id.append(random.nextInt(1,10));
        }
        return Long.parseLong(id.toString());
    }

    public static String randomizeJsonPet() {
        try {
            Pet pet = new Pet(
                    idRandomizer(10),
                    new Category(idRandomizer(10), "Dog"), faker.name().name(),
                    new ArrayList<>(List.of(faker.avatar().image())),
                    new ArrayList<>(List.of(new Tag(idRandomizer(10), faker.dog().breed()))),
                    statusQueryParams
                            .get("status")
                            .get(random.nextInt(3)));
            return objectMapper.writeValueAsString(pet);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String randomizeJsonPetForStore(Long id) {
        String shipDate = "2024-11-11T11:11:11.111+0000";
        try {
            Store store = new Store(
                    random.nextInt(1,11),
                    id,
                    random.nextInt(10),
                    shipDate,
                    statusQueryParams
                            .get("status")
                            .get(random.nextInt(3)),
                    true
            );
            return objectMapper.writeValueAsString(store);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

